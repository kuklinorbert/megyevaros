package com.example.megyevaros.Entity;

import com.google.gson.annotations.SerializedName;

public class Varos {
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;

    public int getId() { return id; }
    public void setId(int value) { this.id = value; }

    public String getName() { return name; }
    public void setName(String value) { this.name = value; }
}

