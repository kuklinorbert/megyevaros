package com.example.megyevaros.Entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VarosResponse {
    @SerializedName("data")
    private List<Varos> data;

    @SerializedName("success")
    private boolean success;

    @SerializedName("errorCode")
    private int errorCode;

    @SerializedName("errorMessage")
    private String errorMessage;

    public List<Varos> getData() {
        return data;
    }

    public void setData(List<Varos> data) {
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
