package com.example.megyevaros.Entity;

import com.google.gson.annotations.SerializedName;

public class Megye {
    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    public Megye(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() { return id; }
    public void setId(int value) { this.id = value; }

    public String getName() { return name; }
    public void setName(String value) { this.name = value; }
}
