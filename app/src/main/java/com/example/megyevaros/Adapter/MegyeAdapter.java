package com.example.megyevaros.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.megyevaros.Entity.Megye;
import com.example.megyevaros.R;

import java.util.List;

public class MegyeAdapter extends ArrayAdapter<Megye> {
    private Context mContext;
    private List<Megye> megyeList;

    public MegyeAdapter(Context context, List<Megye> list){
        super(context, 0, list);
        mContext = context;
        megyeList = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;

        if(listItem == null){
            listItem = LayoutInflater.from(mContext).inflate(R.layout.megye_item,parent,false);
        }

        Megye currentMegye = megyeList.get(position);

        TextView megyeName = listItem.findViewById(R.id.varosNameView);
        megyeName.setText(currentMegye.getName());

        return listItem;
    }
}
