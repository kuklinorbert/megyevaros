package com.example.megyevaros.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.megyevaros.Entity.Varos;
import com.example.megyevaros.R;

import java.util.List;

public class VarosAdapter extends ArrayAdapter<Varos> {
    private Context mContext;
    private List<Varos> varosList;
    private ImageButton modifyButton, deleteButton;
    private ItemClickListener listener;

    public interface ItemClickListener{
        void onDeleteClick(int varosId);
        void onModifyClick(int varosId, String varosName);
    }

    public VarosAdapter(@NonNull Context context, List<Varos> list) {
        super(context, 0, list);

        try{
            listener = (ItemClickListener) context;
        }catch (ClassCastException e) {
            throw new ClassCastException("A meghívó kontextusnak implementálnia kell az ItemClickListener-t!");
        }

        mContext = context;
        varosList = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent){
        View listItem = convertView;

        if(listItem == null){
            listItem = LayoutInflater.from(mContext).inflate(R.layout.varos_item,parent,false);
        }

        Varos currentVaros = varosList.get(position);

        modifyButton = listItem.findViewById(R.id.modifyButton);
        deleteButton = listItem.findViewById(R.id.deleteButton);

        TextView varosName = listItem.findViewById(R.id.varosNameView);
        varosName.setText(currentVaros.getName());

        modifyButton.setOnClickListener(new View.OnClickListener(
        ){
            @Override
            public void onClick(View view) {
                listener.onModifyClick(currentVaros.getId(),currentVaros.getName());
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener(
        ){
            @Override
            public void onClick(View view) {
                listener.onDeleteClick(currentVaros.getId());
            }
        });

        return listItem;
    }
}