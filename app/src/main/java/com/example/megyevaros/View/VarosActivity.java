package com.example.megyevaros.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.database.DataSetObserver;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.megyevaros.Adapter.VarosAdapter;
import com.example.megyevaros.Contract.VarosContract;
import com.example.megyevaros.Entity.Varos;
import com.example.megyevaros.Presenter.VarosPresenter;
import com.example.megyevaros.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class VarosActivity extends AppCompatActivity implements VarosContract.View, AddModifyFragment.DialogListener, VarosAdapter.ItemClickListener {
private VarosAdapter varosAdapter;
private ListView varosView;
private VarosPresenter varosPresenter;
private List<Varos> varosok;
private FloatingActionButton fab;
private ImageButton refreshButton;
private int modifyId, deleteId, megyeId;
private String token;
private ProgressBar progressBar;
private LinearLayout noVaros;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.varos_activity);
        token = getResources().getString(R.string.auth_key);
        fab = findViewById(R.id.floatingActionButton);
        refreshButton = findViewById(R.id.refreshVarosok);
        progressBar = findViewById(R.id.progressBarVaros);
        noVaros = findViewById(R.id.nincsVaros);
        Bundle intent = getIntent().getExtras();
        String megyeName = intent.getString("MEGYE_NAME");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(megyeName);

        megyeId = intent.getInt("MEGYE_ID");
        varosPresenter = new VarosPresenter(this);
        varosPresenter.requestVarosok(token, megyeId);

        fab.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                DialogFragment dialog = new AddModifyFragment(true, "");
                dialog.show(getSupportFragmentManager(), "AddModifyFragment");
            }
        });

        refreshButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                refreshButton.setVisibility(View.INVISIBLE);
                progressBar.setVisibility(View.VISIBLE);
                varosPresenter.requestVarosok(token,megyeId);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setupUI() {
        varosView = findViewById(R.id.varosListView);
        varosok = new ArrayList<>();
        varosAdapter = new VarosAdapter(this, varosok);
        varosView.setAdapter(varosAdapter);
        varosAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                if(varosok.isEmpty()){
                    noVaros.setVisibility(View.VISIBLE);
                }else{
                    noVaros.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    @Override
    public void displayVarosok(List<Varos> varosList) {
        progressBar.setVisibility(View.INVISIBLE);
        if(varosList != null) {
            varosok.addAll(varosList);
            varosAdapter.notifyDataSetChanged();
        }else{
            noVaros.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showMessage(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showButtonAndMessage(String msg) {
        progressBar.setVisibility(View.INVISIBLE);
        refreshButton.setVisibility(View.VISIBLE);
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void addVarosToList(Varos varos) {
        varosok.add(varos);
        varosAdapter.notifyDataSetChanged();
    }

    @Override
    public void modifyVarosInList(Varos varos) {
        int varosId = findVaros(modifyId);
        if(varosId >= 0 ) {
            varosok.set(varosId, varos);
            varosAdapter.notifyDataSetChanged();
        }else{
            showMessage("Hiba a módosításkor!");
        }
    }

    @Override
    public void deleteVarosFromList() {
        int varosId = findVaros(deleteId);
        if(varosId >= 0) {
            varosok.remove(varosId);
            varosAdapter.notifyDataSetChanged();
        }else{
            showMessage("Hiba törléskor!");
        }
    }

    public int findVaros(int id) {
        for (Varos i : varosok) {
            if (i.getId() == id) {
                return varosok.indexOf(i);
            }
        }
        return -1;
    }

    @Override
    public void onDialogActionClick(DialogFragment dialog, boolean addAction, String newName) {
        if(addAction){
            varosPresenter.addVaros(token, newName,megyeId);
        }else{
            varosPresenter.modifyVaros(token, newName,modifyId);
        }
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        dialog.dismiss();
    }

    @Override
    public void onDeleteClick(int varosId) {
        deleteId = varosId;
        varosPresenter.deleteVaros(token, deleteId);
    }

    @Override
    public void onModifyClick(int varosId, String varosName) {
        DialogFragment dialog = new AddModifyFragment(false, varosName);
        modifyId = varosId;
        dialog.show(getSupportFragmentManager(), "AddModifyFragment");
    }
}