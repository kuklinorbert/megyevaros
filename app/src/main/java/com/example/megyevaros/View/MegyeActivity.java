package com.example.megyevaros.View;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.megyevaros.Adapter.MegyeAdapter;
import com.example.megyevaros.Contract.MegyeContract;
import com.example.megyevaros.Entity.Megye;
import com.example.megyevaros.Presenter.MegyePresenter;
import com.example.megyevaros.R;

import java.util.ArrayList;
import java.util.List;

public class MegyeActivity extends AppCompatActivity implements MegyeContract.View {
private MegyeAdapter megyeAdapter;
private ListView megyeView;
private MegyePresenter megyePresenter;
private List<Megye> megyek;
private String token;
private ImageButton refreshButton;
private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.megye_activity);
        token = getResources().getString(R.string.auth_key);
        refreshButton = findViewById(R.id.refreshMegyek);
        progressBar = findViewById(R.id.progressBarMegye);
        megyePresenter = new MegyePresenter(this);
        megyePresenter.getMegyek(token);

        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refreshButton.setVisibility(View.INVISIBLE);
                progressBar.setVisibility(View.VISIBLE);
                megyePresenter.getMegyek(token);
            }
        });
    }

    @Override
    public void setupUI() {
        megyeView = findViewById(R.id.megyeListView);
        megyek = new ArrayList<>();
        megyeAdapter = new MegyeAdapter(this,megyek);
        megyeView.setAdapter(megyeAdapter);
        megyeView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
               int megyeId = megyek.get(i).getId();
               String megyeNev = megyek.get(i).getName();
               navigateToVaros(megyeId, megyeNev);
            }
        });
    }


    @Override
    public void displayMegyek(List<Megye> megyeList) {
        progressBar.setVisibility(View.INVISIBLE);
        megyek.addAll(megyeList);
        megyeAdapter.notifyDataSetChanged();
    }

    @Override
    public void showMessage(String msg) {
        progressBar.setVisibility(View.INVISIBLE);
        refreshButton.setVisibility(View.VISIBLE);
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }

    public void navigateToVaros(int megyeId,String megyeName){
        Intent intent = new Intent(this, VarosActivity.class);
        intent.putExtra("MEGYE_ID", megyeId);
        intent.putExtra("MEGYE_NAME", megyeName);
        startActivity(intent);
    }

}