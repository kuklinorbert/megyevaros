package com.example.megyevaros.View;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.megyevaros.R;

public class AddModifyFragment extends DialogFragment {

    private boolean addVaros;
    private String varosNev;

    public interface DialogListener{
         void onDialogActionClick(DialogFragment dialog, boolean addAction, String newName);
         void onDialogNegativeClick(DialogFragment dialog);
    }

    public AddModifyFragment(boolean addVaros, String varosNev){
        this.addVaros = addVaros;
        this.varosNev = varosNev;
    }

    DialogListener listener;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog,null);

        builder.setView(view).setPositiveButton(addVaros ? "Hozzáad" : "Módosít", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                EditText editText = view.findViewById(R.id.dialogText);
                listener.onDialogActionClick(AddModifyFragment.this, addVaros, editText.getText().toString());
            }
        }).setNegativeButton("Mégse", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                listener.onDialogNegativeClick(AddModifyFragment.this);            }
        });

        TextView test = view.findViewById(R.id.dialogName);
        test.setText( addVaros ?  "Új város hozzáadása" : "Város módosítása");
        EditText test1 = view.findViewById(R.id.dialogText);
        test1.setText(varosNev);

        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try{
            listener = (DialogListener) context;
        }catch (ClassCastException e){
            throw new ClassCastException(getActivity().toString() + " implementálnia kell az DialogListener-t!");
        }
    }
}