package com.example.megyevaros.Contract;

import com.example.megyevaros.Entity.Megye;

import java.util.List;

public interface MegyeContract {

    interface Model {

        interface MegyeListener{
            void onSuccess(List<Megye> megyeList);
            void onFailure(Throwable t);
        }

        void getMegyeList(final MegyeListener listener, String token);

    }

    interface View {

        void setupUI();
        void displayMegyek(List<Megye> megyeList);
        void showMessage(String msg);

    }

    interface Presenter {
        void getMegyek(String token);
    }



}
