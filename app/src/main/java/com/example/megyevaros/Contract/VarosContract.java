package com.example.megyevaros.Contract;

import com.example.megyevaros.Entity.Varos;

import java.util.List;

public interface VarosContract {

    interface Model {
        interface GetVarosListener {
            void onGetSuccess(List<Varos> varos);
            void onGetFailure(Throwable t);
        }

        interface AddModifyVarosListener{
            void onAddModifySuccess(Varos response, boolean action);
            void onAddModifyFailure(Throwable t);
        }

        interface DeleteVarosListener{
            void onDeleteSuccess();
            void onDeleteError(Throwable t);
        }

        void getVarosList(final GetVarosListener listener, String token, int megyeId);
        void addVarosPUT(final AddModifyVarosListener listener, String token, String varosNev, int megyeId);
        void modifyVarosPATCH(final AddModifyVarosListener listener, String token, String varosName, int varosId);
        void deleteVarosDELETE(final DeleteVarosListener listener, String token, int varosId);
    }

    interface View{
        void setupUI();
        void displayVarosok(List<Varos> varosList);
        void showMessage(String msg);
        void showButtonAndMessage(String msg);
        void addVarosToList(Varos varos);
        void modifyVarosInList(Varos varos);
        void deleteVarosFromList();
    }

    interface Presenter {
        void requestVarosok(String token, int megyeId);
        void addVaros(String token, String varosName, int megyeId);
        void modifyVaros(String token, String varosName, int varosId);
        void deleteVaros(String token, int varosId);
    }

}
