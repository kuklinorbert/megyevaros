package com.example.megyevaros.Presenter;

import com.example.megyevaros.Contract.MegyeContract;
import com.example.megyevaros.Entity.Megye;
import com.example.megyevaros.Model.MegyeModel;
import java.util.List;


public class MegyePresenter implements MegyeContract.Presenter,MegyeContract.Model.MegyeListener {

    private MegyeContract.View mView;
    private MegyeContract.Model mModel;

    public MegyePresenter(MegyeContract.View view){
        mView = view;
        mModel = new MegyeModel();
        mView.setupUI();
    }

    @Override
    public void getMegyek(String token) {
        mModel.getMegyeList(this, token);
    }


    @Override
    public void onSuccess(List<Megye> megyeList) {
        mView.displayMegyek(megyeList);
    }

    @Override
    public void onFailure(Throwable t) {
        mView.showMessage(t.getMessage());
    }
}
