package com.example.megyevaros.Presenter;

import com.example.megyevaros.Contract.VarosContract;
import com.example.megyevaros.Entity.Varos;
import com.example.megyevaros.Model.VarosModel;

import java.util.List;


public class VarosPresenter implements VarosContract.Presenter, VarosContract.Model.GetVarosListener,VarosContract.Model.AddModifyVarosListener,VarosContract.Model.DeleteVarosListener {

    private VarosContract.View vView;
    private VarosContract.Model vModel;

    public VarosPresenter(VarosContract.View view){
        vView = view;
        vModel = new VarosModel();
        vView.setupUI();
    }

    @Override
    public void requestVarosok(String token, int megyeId) {
        vModel.getVarosList(this,token, megyeId);
    }

    @Override
    public void addVaros(String token, String varosName, int megyeId) {
        vModel.addVarosPUT(this,token,varosName,megyeId);
    }

    @Override
    public void modifyVaros(String token, String varosName, int varosId) {
        vModel.modifyVarosPATCH(this, token, varosName,varosId);
    }

    @Override
    public void deleteVaros(String token, int varosId) {
        vModel.deleteVarosDELETE(this, token, varosId);
    }

    @Override
    public void onGetSuccess(List<Varos> varosList) {
        vView.displayVarosok(varosList);
    }

    @Override
    public void onGetFailure(Throwable t) {
        vView.showButtonAndMessage(t.getMessage());
    }

    @Override
    public void onAddModifySuccess(Varos response, boolean action) {
        if(action) {
            vView.addVarosToList(response);
        }else{
            vView.modifyVarosInList(response);
        }
    }

    @Override
    public void onAddModifyFailure(Throwable t) {
        vView.showMessage(t.getMessage());
    }


    @Override
    public void onDeleteSuccess() {
        vView.deleteVarosFromList();
    }

    @Override
    public void onDeleteError(Throwable t) {
        vView.showMessage(t.getMessage());
    }

}
