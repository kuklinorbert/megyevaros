package com.example.megyevaros.Network;

import com.example.megyevaros.Entity.MegyeResponse;
import com.example.megyevaros.Entity.VarosResponse;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;

public interface ApiInterface {

    @GET("all_states")
    Call<MegyeResponse> getMegyek(@Header("token") String apiKey);

   @FormUrlEncoded
    @POST("state_city")
    Call<VarosResponse> getVarosok(@Header("token") String apiKey, @Field("state_id") int megyeId);

    @FormUrlEncoded
    @PUT("city")
    Call<JsonObject> addVaros(@Header("token") String apiKey, @Field("name") String varosNev, @Field("state_id") int megyeId);

    @FormUrlEncoded
    @PATCH("city")
    Call<JsonObject> modifyVaros(@Header("token") String apiKey, @Field("name") String varosNev, @Field("city_id") int megyeId);

   @FormUrlEncoded
   @HTTP(method = "DELETE", path = "city", hasBody = true)
    Call<JsonObject> deleteVaros(@Header("token") String apiKey, @Field("city_id") int megyeId);
}