package com.example.megyevaros.Network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    private static Retrofit retrofit = null;

    public static Retrofit getClient() {
        if(retrofit == null){
            retrofit = new Retrofit.Builder().baseUrl("https://probafeladat-api.zengo.eu/api/").addConverterFactory(GsonConverterFactory.create()).build();
        }
        return retrofit;
    }
}
