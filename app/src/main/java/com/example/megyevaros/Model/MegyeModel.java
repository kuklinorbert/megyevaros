package com.example.megyevaros.Model;

import com.example.megyevaros.Contract.MegyeContract;
import com.example.megyevaros.Entity.Megye;
import com.example.megyevaros.Entity.MegyeResponse;
import com.example.megyevaros.Network.ApiClient;
import com.example.megyevaros.Network.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
public class MegyeModel implements MegyeContract.Model {

    @Override
    public void getMegyeList(MegyeListener listener, String token) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<MegyeResponse> call = apiService.getMegyek(token);
        call.enqueue(new Callback<MegyeResponse>() {
            @Override
            public void onResponse(Call<MegyeResponse> call, Response<MegyeResponse> response) {
                if(response.isSuccessful()){
                    List<Megye> megyek = response.body().getData();
                    listener.onSuccess(megyek);
                }else{
                    String errorMsg = response.code() + " - " + response.message();
                    listener.onFailure(new Throwable(errorMsg));
                }
            }

            @Override
            public void onFailure(Call<MegyeResponse> call, Throwable t) {
                listener.onFailure(t);
            }
        });
    }

}
