package com.example.megyevaros.Model;

import com.example.megyevaros.Contract.VarosContract;
import com.example.megyevaros.Entity.Varos;
import com.example.megyevaros.Entity.VarosResponse;
import com.example.megyevaros.Network.ApiClient;
import com.example.megyevaros.Network.ApiInterface;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VarosModel implements VarosContract.Model {

    @Override
    public void getVarosList(GetVarosListener listener, String token, int megyeId) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<VarosResponse> call = apiService.getVarosok(token, megyeId);
        call.enqueue(new Callback<VarosResponse>() {
            @Override
            public void onResponse(Call<VarosResponse> call, Response<VarosResponse> response) {
                if(response.isSuccessful()){
                        List<Varos> varosok = response.body().getData();
                        listener.onGetSuccess(varosok);
                }else{
                    String errorMsg;
                    try{
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        errorMsg = jsonObject.getString("errorMessage");
                    }catch (Exception e)
                    {
                        errorMsg = response.code() + " - " + response.message();
                    }
                    listener.onGetFailure(new Throwable(errorMsg));
                }
            }
            @Override
            public void onFailure(Call<VarosResponse> call, Throwable t) {
                listener.onGetFailure(t);
            }
        });
    }


    @Override
    public void addVarosPUT(AddModifyVarosListener listener, String token, String varosNev, int megyeId) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<JsonObject> call = apiService.addVaros(token, varosNev,megyeId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if(response.isSuccessful()){
                    if (response.body().get("success").getAsBoolean()){
                        Gson gson = new Gson();
                        Varos varos = gson.fromJson(response.body().get("data"),Varos.class);
                        listener.onAddModifySuccess(varos, true);
                    }else{
                        listener.onAddModifyFailure(new Throwable(response.body().get("errorMessage").getAsJsonObject().get("name").getAsString()));
                    }
                }else{
                    String errorMsg;
                    try{
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        errorMsg = jsonObject.getString("errorMessage");
                    }catch (Exception e)
                    {
                        errorMsg = response.code() + " - " + response.message();
                    }
                    listener.onAddModifyFailure(new Throwable(errorMsg));
                }

            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                listener.onAddModifyFailure(t);
            }
        });
    }

    @Override
    public void modifyVarosPATCH(AddModifyVarosListener listener, String token, String varosName, int varosId) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<JsonObject> call = apiService.modifyVaros(token,varosName,varosId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if(response.isSuccessful()) {
                    if (response.body().get("success").getAsBoolean()) {
                        Gson gson = new Gson();
                        Varos varos = gson.fromJson(response.body().get("data"), Varos.class);
                        listener.onAddModifySuccess(varos, false);
                    } else {
                        listener.onAddModifyFailure(new Throwable(response.body().get("errorMessage").getAsJsonObject().get("name").getAsString()));
                    }
                }else{
                    String errorMsg;
                    try{
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        errorMsg = jsonObject.getString("errorMessage");
                    }catch (Exception e)
                    {
                        errorMsg = response.code() + " - " + response.message();
                    }
                    listener.onAddModifyFailure(new Throwable(errorMsg));
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                listener.onAddModifyFailure(t);
            }
        });
    }

    @Override
    public void deleteVarosDELETE(DeleteVarosListener listener, String token, int varosId) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<JsonObject> call = apiService.deleteVaros(token, varosId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if(response.isSuccessful()){
                    listener.onDeleteSuccess();
                }else{
                    String errorMsg;
                    try{
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        errorMsg = jsonObject.getString("errorMessage");
                    }catch (Exception e)
                    {
                        errorMsg = response.code() + " - " + response.message();
                    }
                    listener.onDeleteError(new Throwable(errorMsg));
                }
            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                listener.onDeleteError(new Throwable(t));
            }
        });
    }

}
